package com.anriku.gradleplugin

import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * created by wenyipeng on 2019-05-12
 */
class HelloPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.task("hello", type: HelloTask) { task ->
            doLast {
                println task.greeting + "," + task.people
            }
        }
    }

}
