package com.anriku.gradleplugin

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

/**
 * created by wenyipeng on 2019-05-12
 */
class HelloTask extends DefaultTask {

    @Input String greeting

    @Input String people

    @TaskAction
    void greet() {
        println "$greeting, $people"
    }

}
